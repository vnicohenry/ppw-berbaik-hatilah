from django.urls import path
from . import views

app_name = 'story4app'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('form/', views.form, name='form'),
    path('schedule/', views.schedule, name='schedule'),
    path('form/<id>', views.delete_jadwal, name="delete")
    
    # dilanjutkan ... okee
]
