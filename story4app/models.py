from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Schedule(models.Model):
    hari = models.CharField(max_length=10)
    tanggal = models.DateTimeField(default=datetime.now) 
    jam = models.TimeField(default=datetime.now)
    nama_kegiatan = models.CharField(max_length=100);
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
    


# Create your models here.
