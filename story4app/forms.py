from . import models
from django import forms
from datetime import datetime, date

class ScheduleForm(forms.Form):
	hari = forms.CharField(
		widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control'})
							)
	tanggal = forms.DateField(
        widget=forms.DateInput(attrs={'required' : 'True' , 'type':'date',
            'class': 'form-control'}))
			
	jam = forms.TimeField(widget=forms.TimeInput(attrs={'required' : 'True' , 'type':'time' , 'class':'timepicker'}))

	nama_kegiatan = forms.CharField(
		widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control'})
							)
	tempat = forms.CharField(
		widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control'})
							)
	kategori = forms.CharField(
		widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control'})
							)

