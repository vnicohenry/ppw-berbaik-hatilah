from django.shortcuts import render, redirect
from .models import Schedule
from . import forms


# Create your views here.
def home(request):
    return render(request, 'home.html')

def aboutme(request):
	data = Schedule.objects.all()
	return render(request, 'aboutme.html', {"data": data})

def form(request):
	if request.method == "POST":
		form = forms.ScheduleForm(request.POST)
		if form.is_valid():
			hari = form.cleaned_data["hari"]
			tanggal = form.cleaned_data["tanggal"]
			nama_kegiatan = form.cleaned_data["nama_kegiatan"]
			jam = form.cleaned_data["jam"]
			tempat = form.cleaned_data["tempat"]
			kategori = form.cleaned_data["kategori"]
			jadwal = Schedule.objects.create(
				hari=hari,
				tanggal= tanggal,
				nama_kegiatan=nama_kegiatan,
				jam=jam,
				tempat=tempat,
				kategori=kategori,
				)
			jadwal.save()
			return redirect("story4app:schedule")
	form = forms.ScheduleForm()
	data = Schedule.objects.all()
	return render(request, 'form.html', {
		"form": form,
		"data": data
		}
	)

def delete_jadwal(request,id):
	data = Schedule.objects.get(id=id)
	data.delete()
	return redirect("story4app:schedule")

def schedule(request):
	data = Schedule.objects.all()
	return render(request, 'schedule.html', {"data": data})

# Create your views here.
